package common;

public class Constants {

    public static final String USER_DIR = System.getProperty("user.dir");
    public static final String TEST_RESOURCES_DIR = USER_DIR + "/src/test/resources/";
    public static final String MAIN_RESOURCES_DIR = USER_DIR + "/src/main/resources/";
    public static final String SELENIUM_PROPERTIES_FILE = MAIN_RESOURCES_DIR + "selenium.properties";
    public static final String FILE_DOWNLOAD_DIR = USER_DIR + "/test-output/downloads";

    public static final String ENVIRONMENT = System.getProperty("env", "dev").toLowerCase();
}
