package common.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import java.util.HashMap;
import java.util.Map;

public class ExtentTestManager {

    private static Map<Integer, ExtentTest> extentTestClassMap = new HashMap<>();
    static Map<Integer, ExtentTest> extentTestMap = new HashMap<>();
    static ExtentReports extent = ExtentManager.createExtentReports();

    public static void log(String message, Status status){
        getTest().log(status, message);
    }

    public static void log(String message){
        log(message, Status.INFO);
    }
    public static synchronized ExtentTest getTest() {
        return extentTestMap.get((int) Thread.currentThread().getId());
    }

    public static synchronized ExtentTest createTestNode(String testClass, String testName, String desc) {
        if (getTestClassNode() != null) {
            if (getTestClassNode().getModel().getName().equalsIgnoreCase(testClass)) {
                ExtentTest test = getTestClassNode().createNode(testName);
                extentTestMap.put((int) Thread.currentThread().getId(), test);
                return test;
            } else {
                ExtentTest test = startTest(testClass, "").createNode(testName);
                extentTestMap.put((int) Thread.currentThread().getId(), test);
                return test;
            }
        } else {
            ExtentTest test = startTest(testClass, "").createNode(testName);
            extentTestMap.put((int) Thread.currentThread().getId(), test);
            return test;
        }
    }

    private static synchronized ExtentTest startTest(String className, String desc) {
        ExtentTest test = extent.createTest(className, desc);
        extentTestClassMap.put((int) Thread.currentThread().getId(), test);
        return test;
    }

    private static synchronized ExtentTest getTestClassNode() {
        return extentTestClassMap.get((int) Thread.currentThread().getId());
    }
}
