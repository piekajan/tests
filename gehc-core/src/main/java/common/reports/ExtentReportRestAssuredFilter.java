package common.reports;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.CodeLanguage;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import java.util.Optional;

public class ExtentReportRestAssuredFilter implements Filter {
    @Override
    public Response filter(FilterableRequestSpecification filterableRequestSpecification, FilterableResponseSpecification filterableResponseSpecification, FilterContext filterContext) {
        Response response = filterContext.next(filterableRequestSpecification, filterableResponseSpecification);
        ExtentTest test = ExtentTestManager.getTest();
        if (test != null) {
            test.log(Status.INFO, "Request method: " + filterableRequestSpecification.getMethod());
            test.log(Status.INFO, "Request URI: " + filterableRequestSpecification.getURI());
            test.log(Status.INFO, "Request body: ");
            Optional.ofNullable(filterableRequestSpecification.getBody()).ifPresent(body -> {
                test.info(MarkupHelper.createCodeBlock(filterableRequestSpecification.getBody(), CodeLanguage.JSON));
            });
            test.log(Status.INFO, "=========================");
            test.log(Status.INFO, "Response code: " + response.getStatusCode());
            test.log(Status.INFO, "Response body: ");
            test.info(MarkupHelper.createCodeBlock(response.asPrettyString(), CodeLanguage.JSON));
        }
        return response;
    }
}
