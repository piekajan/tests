package common.logger;


import org.apache.log4j.Logger;

public class AutomationLogger {

  public static Logger log = getLogger();

  public static synchronized Logger getLogger() {
    return Logger.getLogger(String.valueOf(Thread.currentThread().getId()));
  }
}
