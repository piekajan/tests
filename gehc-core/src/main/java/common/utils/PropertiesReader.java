package common.utils;


import groovy.lang.MissingPropertyException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.Properties;

public class PropertiesReader {

    private Properties properties;
    private File file;

    public PropertiesReader(String filepath) {
        try {
            file = new File(filepath);
            FileInputStream input = new FileInputStream(file);
            properties = new Properties();
            properties.load(new InputStreamReader(input, StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getProperty(String key) {
        return Optional.ofNullable(properties.getProperty(key))
                .filter(value -> !value.isEmpty())
                .orElseThrow(() -> new MissingPropertyException(key + " property not found in file " + file.getName()));
    }
}
