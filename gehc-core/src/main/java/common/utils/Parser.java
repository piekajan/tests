package common.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public class Parser {

  public static <T> T mapToClass(Map map, Type clazz) {
    Gson gson = new Gson();
    JsonElement jsonElement = gson.toJsonTree(map);
    return gson.fromJson(jsonElement, clazz);
  }

  public static String fileToString(String filePath) {
    try {
      return new String(Files.readAllBytes(Paths.get(filePath)));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
