package common.utils.test_data_container;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

public class TestDataContainer {

  private static Map<String, Object> testDataContainer = new HashMap<>();

  public static synchronized void saveTestData(TestData key, Object data) {
    testDataContainer.put(key.name(), data);
  }

  public static synchronized Object getTestData(TestData key) {
    return Optional.ofNullable(testDataContainer.get(key.name()))
        .orElseThrow(
            () ->
                new NoSuchElementException(
                    "The key " + key.name() + " is not present in test data container"));
  }

  public static synchronized <T> T getTestData(TestData key, Class<T> type) {
    try {
      return type.cast(getTestData(key));
    } catch (ClassCastException e) {
      throw new ClassCastException(
          key.name() + " is not of type " + type.getTypeName() + "\n" + e.getMessage());
    }
  }
}
