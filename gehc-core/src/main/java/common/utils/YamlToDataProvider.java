package common.utils;

import common.Constants;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.yaml.snakeyaml.Yaml;

public class YamlToDataProvider {

    public enum Suite {
        UI, API
    }


    public static Object[][] getTestData(String className, String methodName, Suite suite) {
        try {
            Yaml yaml = new Yaml();
            String path = Constants.TEST_RESOURCES_DIR + suite.toString().toLowerCase() + "/" + className + ".yaml";
            InputStream inputStream = Files.newInputStream(new File(path).toPath());
            LinkedHashMap<String, ArrayList> fileData = yaml.load(inputStream);
            ArrayList data = fileData.get(methodName);
            Object[][] obj = new Object[data.size()][1];
            for (int i = 0; i < data.size(); i++) {
                obj[i][0] = data.get(i);
            }
            return obj;
        } catch (Exception e) {
            throw new RuntimeException("Failed to load test data for " + className + "/" + methodName + ":\n" + e);
        }
    }
}
