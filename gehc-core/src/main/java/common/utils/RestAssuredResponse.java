package common.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonParseException;
import io.restassured.response.Response;
import java.time.LocalDate;
import java.time.OffsetDateTime;

public class RestAssuredResponse {

    private static Gson gson = new GsonBuilder()
            .registerTypeAdapter(OffsetDateTime.class, (JsonDeserializer<OffsetDateTime>)
                    (json, type, context) -> OffsetDateTime.parse(json.getAsString()))
            .registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>)
                    (json, type, context) -> LocalDate.parse(json.getAsString()))
            .create();

    public static <T> T as(Response response, Class<T> type) {
        try {
            if (response.body().asString().isEmpty()) {
                return null;
            } else {
                return response.as(type);
            }
        } catch (Exception e) {
            throw new JsonParseException("\nJson response body: \n" + response.asPrettyString() + " \n cannot be converted to " + type.getTypeName() + "\n" + e.getMessage());
        }
    }
}
