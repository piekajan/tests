package common.utils;

import static common.Constants.MAIN_RESOURCES_DIR;

public class EnvironmentData {

  private static final String ENV_CONFIG_FILE = MAIN_RESOURCES_DIR + "environment.properties";
  private static final PropertiesReader propertiesReader;

  static {
    propertiesReader = new PropertiesReader(ENV_CONFIG_FILE);
  }

  public static String getWebUrl() {
    return propertiesReader.getProperty("website.url");
  }

  public static String getApiUrl() {
    return propertiesReader.getProperty("api.url");
  }

  public static String getDefaultUsername() {
    return propertiesReader.getProperty("username");
  }

  public static String getDefaultPassword() {
    return propertiesReader.getProperty("password");
  }
}
