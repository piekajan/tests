package common.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public class CsvCreator {

    private CSVPrinter csvPrinter;
    private String filename;

    public CsvCreator(String filename, String[] headers) {
        try {
            this.filename = filename;
            csvPrinter = new CSVPrinter(new FileWriter(this.filename), CSVFormat.EXCEL.builder().setHeader(headers).build());
        } catch (IOException e) {
            throw new RuntimeException("Failed to initialize csv creator " + e);
        }
    }

    public CsvCreator addRecord(String[] records) {
        try {
            csvPrinter.printRecord(records);
            return this;
        } catch (IOException e) {
            throw new RuntimeException("Failed to add new record to file " + e);
        }
    }

    public File build() {
        try {
            csvPrinter.flush();
            csvPrinter.close();
            return new File(filename);
        } catch (IOException e) {
            throw new RuntimeException("Failed to save csv file " + e);
        }
    }
}
