package common.utils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import org.apache.commons.codec.Charsets;
import org.apache.commons.lang3.RandomStringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;

public class DataGenerator {

    private static final EasyRandom easyRandom = new EasyRandom(new EasyRandomParameters()
            .seed(123L)
            .objectPoolSize(100)
            .randomizationDepth(10)
            .charset(Charsets.UTF_8)
            .timeRange(LocalTime.now().minus(1, ChronoUnit.HOURS), LocalTime.now().plus(1, ChronoUnit.HOURS))
            .dateRange(LocalDate.now().minus(100, ChronoUnit.DAYS), LocalDate.now().plus(100, ChronoUnit.DAYS))
            .stringLengthRange(5, 10)
            .collectionSizeRange(1, 3)
            .scanClasspathForConcreteTypes(true)
            .overrideDefaultInitialization(false)
            .ignoreRandomizationErrors(true));

    public static String randomAlhpanumeric(int length){
        return RandomStringUtils.random(length, true, true);
    }

    public static String randomAlhpanumeric(){
        return RandomStringUtils.random(15, true, true);
    }

    public static String randomAlhpanumericWithPrefix(int length, String prefix){
        return prefix + RandomStringUtils.random(length, true, true);
    }

    public static String randomIntString(int length){
        return String.valueOf(new Random().nextInt(length));
    }

    public static Integer parseInt(String pageSize) {
        if (pageSize != null) {
            return Integer.parseInt(pageSize);
        } else {
            return null;
        }
    }

    public static <T> T getRandomObject(Class<T> type) {
        return easyRandom.nextObject(type);
    }
}
