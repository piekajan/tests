package common.listeners;

import com.aventstack.extentreports.Status;
import common.reports.ExtentManager;
import common.reports.ExtentTestManager;
import java.util.Arrays;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {

    @Override
    public void onStart(ITestContext iTestContext) {
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        ExtentManager.extentReports.flush();
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        String testName = iTestResult.getName();
        if (iTestResult.getParameters().length > 0) {
            testName = testName + " " + iTestResult.getParameters()[0].toString();
        }
        ExtentTestManager.createTestNode(iTestResult.getTestClass().getRealClass().getSimpleName(), testName, "");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        ExtentTestManager.getTest().log(Status.PASS, "Test passed");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        ExtentTestManager.getTest().log(Status.FAIL, iTestResult.getThrowable());
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Arrays.stream(iTestResult.getThrowable().getStackTrace())
                .filter(t -> t.getMethodName().toLowerCase().contains("preparedata"))
                .findFirst()
                .ifPresent(t -> ExtentTestManager.getTest().log(Status.SKIP, "Prepare data @Before method has failed"));
        ExtentTestManager.getTest().log(Status.SKIP, iTestResult.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    }
}
