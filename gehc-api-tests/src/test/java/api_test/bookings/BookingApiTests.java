package api_test.bookings;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import api.base.ConfigurationBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BookingApiTests {

  private String json =
      "{\n"
          + "    \"firstname\" : \"Jim\",\n"
          + "    \"lastname\" : \"Brown\",\n"
          + "    \"totalprice\" : 111,\n"
          + "    \"depositpaid\" : true,\n"
          + "    \"bookingdates\" : {\n"
          + "        \"checkin\" : \"2018-01-01\",\n"
          + "        \"checkout\" : \"2019-01-01\"\n"
          + "    },\n"
          + "    \"additionalneeds\" : \"Breakfast\"\n"
          + "}";

  private int id;

  @BeforeClass
  public void prepareData() {
    ConfigurationBuilder.setupClient();
  }

  @Test
  public void shouldCreateBooking() {
    id =
        given()
            .body(json)
            .contentType(ContentType.JSON)
            .post("/booking")
            .then()
            .statusCode(200)
            .body("booking", equalTo(new JsonPath(json).getMap("")))
            .extract()
            .jsonPath()
            .getInt("bookingid");
  }

  @Test(dependsOnMethods = "shouldCreateBooking")
  public void shouldGetCreatedBooking() {
    given()
        .get("/booking/" + id)
        .then()
        .statusCode(200)
        .body("", equalTo(new JsonPath(json).getMap("")));
  }

  @Test(dependsOnMethods = {"shouldGetCreatedBooking", "shouldCreateBooking"})
  public void shouldUpdateCreatedBooking() {
    json = json.replaceAll("Jim", "Jim2");
    given()
        .body(json)
        .contentType(ContentType.JSON)
        .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
        .put("/booking/" + id)
        .then()
        .statusCode(200)
        .body("", equalTo(new JsonPath(json).getMap("")));
  }

  @Test(
      dependsOnMethods = {
        "shouldUpdateCreatedBooking",
        "shouldGetCreatedBooking",
        "shouldCreateBooking"
      })
  public void shouldDeleteCreatedBooking() {
    given()
        .body(json)
        .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
        .delete("/booking/" + id)
        .then()
        .statusCode(201);
  }

  @Test(
      dependsOnMethods = {
        "shouldUpdateCreatedBooking",
        "shouldGetCreatedBooking",
        "shouldCreateBooking",
        "shouldDeleteCreatedBooking"
      })
  public void shouldNotSeeCreatedBooking() {
    given().body(json).get("/booking/" + id).then().statusCode(404);
  }
}
