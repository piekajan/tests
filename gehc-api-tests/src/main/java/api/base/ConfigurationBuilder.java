package api.base;


import common.utils.EnvironmentData;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;

public class ConfigurationBuilder {

    public static void setupClient() {
        RestAssured.baseURI = EnvironmentData.getApiUrl();
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }
}
