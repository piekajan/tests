package ui_test.base;

import common.utils.EnvironmentData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Browser;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import ui.config.DriverFactory;

public class BaseTest {

    protected WebDriver driver;
    protected final String url = EnvironmentData.getWebUrl();

    @BeforeClass
    public void startBrowser(){
        driver = DriverFactory.startDriver(Browser.CHROME);
        driver.manage().window().maximize();
        driver.get(url);
    }

    @AfterClass(alwaysRun = true)
    public void quitBrowser(){
        if (driver != null){
            driver.quit();
        }
    }
}
