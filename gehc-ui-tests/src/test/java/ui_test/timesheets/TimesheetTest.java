package ui_test.timesheets;

import common.utils.EnvironmentData;
import common.utils.YamlToDataProvider;
import org.testng.annotations.*;
import ui.pages.HomePage;
import ui.pages.LoginPage;
import ui_test.base.BaseTest;

import java.lang.reflect.Method;
import java.util.Map;

public class TimesheetTest extends BaseTest {

  @DataProvider(name = "TimesheetTest")
  public Object[][] dataProvider(Method method) {
    return YamlToDataProvider.getTestData(this.getClass().getSimpleName(), method.getName(),
            YamlToDataProvider.Suite.UI);
  }

  @BeforeMethod
  public void login() {
    new LoginPage(driver)
        .login(EnvironmentData.getDefaultUsername(), EnvironmentData.getDefaultPassword())
        .verify()
        .shouldBeLoggedIn();
  }

  @Test(dataProvider = "TimesheetTest")
  public void shouldSeeCommentOnTimesheet(Map<String, String> testData) {
    new HomePage(driver)
        .goToTime()
        .viewTimesheetFor(testData.get("period"))
        .openCommentAtIndex(Integer.parseInt(testData.get("timesheetAtIndex")))
        .verify()
        .shouldSeeComment(testData.get("expectedComment"));
  }
}
