package ui.assertions;

import static org.assertj.core.api.Assertions.assertThat;

import org.openqa.selenium.WebDriver;
import ui.pages.HomePage;

public class HomePageAssertions extends HomePage {

  public HomePageAssertions(WebDriver driver) {
    super(driver);
  }

  public HomePageAssertions shouldBeLoggedIn() {
    assertThat(isDisplayedWithTimeout(userDropdown))
        .as("User should be logged in (user dropdown displayed)")
        .isTrue();
    return this;
  }
}
