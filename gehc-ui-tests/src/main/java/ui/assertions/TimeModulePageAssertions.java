package ui.assertions;

import static org.assertj.core.api.Assertions.assertThat;

import common.logger.AutomationLogger;
import org.openqa.selenium.WebDriver;
import ui.pages.TimeModulePage;

public class TimeModulePageAssertions extends TimeModulePage {

  public TimeModulePageAssertions(WebDriver driver) {
    super(driver);
  }

  public TimeModulePageAssertions shouldSeeComment(String expectedComment){
    waitForStableHtml();
    String comment = getAttribute(commentContents, "value");
    AutomationLogger.log.info("Comment is: " + comment);
    assertThat(comment)
            .as("Comment should match expected")
            .isEqualTo(expectedComment);
    return this;
  }


}
