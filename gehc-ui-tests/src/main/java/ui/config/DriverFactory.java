package ui.config;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.Browser;
import org.openqa.selenium.safari.SafariDriver;

public class DriverFactory {

    public static WebDriver startDriver(Browser browser) {
        if (browser.is(Browser.CHROME.browserName())) {
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver(CapabilitiesProvider.getChromeOptions());
        } else if (browser.is(Browser.FIREFOX.browserName())) {
            WebDriverManager.firefoxdriver().setup();
            return new FirefoxDriver(CapabilitiesProvider.getFirefoxOptions());
        } else if (browser.is(Browser.SAFARI.browserName())) {
            WebDriverManager.safaridriver().setup();
            return new SafariDriver();
        } else {
            throw new WebDriverException(browser.browserName() + " is not possible option. Please choose between: Chrome, Firefox, Safari");
        }
    }
}
