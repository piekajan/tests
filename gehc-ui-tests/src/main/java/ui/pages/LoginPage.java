package ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.assertions.LoginPageAssertions;
import ui.pages.base.BasePage;

public class LoginPage extends BasePage<LoginPageAssertions> {

  @FindBy(css = "input[name='username']")
  protected WebElement usernameInput;

  @FindBy(css = "input[name='password']")
  protected WebElement passwordInput;

  @FindBy(css = "button[type='submit']")
  protected WebElement loginButton;

  public LoginPage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  public HomePage login(String username, String password) {
    writeText(username, usernameInput);
    writeText(password, passwordInput);
    clickOn(loginButton);
    return new HomePage(driver);
  }

  @Override
  public LoginPageAssertions verify() {
    return new LoginPageAssertions(driver);
  }
}
