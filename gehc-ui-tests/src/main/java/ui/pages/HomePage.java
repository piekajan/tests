package ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import ui.assertions.HomePageAssertions;
import ui.pages.base.BasePage;
import ui.pages.base.RangeHrmBasePage;

public class HomePage extends RangeHrmBasePage<HomePageAssertions> {

  public HomePage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  @Override
  public HomePageAssertions verify() {
    return new HomePageAssertions(driver);
  }
}
