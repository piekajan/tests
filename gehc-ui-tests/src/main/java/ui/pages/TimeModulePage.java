package ui.pages;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.assertions.TimeModulePageAssertions;
import ui.pages.base.RangeHrmBasePage;

public class TimeModulePage extends RangeHrmBasePage<TimeModulePageAssertions> {

  @FindBy(css = "div.oxd-table-card")
  protected List<WebElement> timesheetRows;
  @FindBy(xpath="//button[contains(@class, 'comment')]")
  protected List<WebElement> commentButtons;
  @FindBy(css = "textarea.oxd-textarea--active")
  protected WebElement commentContents;


  public TimeModulePage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  public TimeModulePage viewTimesheetFor(String period) {
    waitForVisibilityOfAtLeast(timesheetRows, 1).stream()
        .filter(e -> e.getText().contains(period))
        .findFirst()
        .orElseThrow(() -> new NoSuchElementException("Cannot find timesheet for period " + period))
        .findElement(By.tagName("button"))
        .click();
    return this;
  }

  public TimeModulePage openCommentAtIndex(int index){
    getOnlyDisplayedElementsOfList(commentButtons)
            .get(index)
            .click();
    return this;
  }

  @Override
  public TimeModulePageAssertions verify() {
    return new TimeModulePageAssertions(driver);
  }
}
