package ui.pages.base;

import static common.Constants.SELENIUM_PROPERTIES_FILE;
import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.primitives.Chars;
import common.logger.AutomationLogger;
import common.utils.PropertiesReader;
import java.io.File;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.json.JsonException;
import org.openqa.selenium.support.ui.*;
import ui.tools.Retry;

public abstract class BasePage<V> {

  protected WebDriver driver;
  protected WebDriverWait wait;
  protected JavascriptExecutor jsExecutor;
  private final long waitTimeout =
      Long.parseLong(new PropertiesReader(SELENIUM_PROPERTIES_FILE).getProperty("wait.timeout"));

  protected BasePage(WebDriver driver) {
    this.driver = driver;
    this.wait = new WebDriverWait(driver, Duration.ofSeconds(waitTimeout));
    jsExecutor = ((JavascriptExecutor) driver);
  }

  protected WebElement clickOn(WebElement element) {
    try {
      element.click();
    } catch (Exception e) {
      waitForClickable(element).click();
    }
    return element;
  }

  protected WebElement clickOn(By by) {
    WebElement element;
    try {
      element = driver.findElement(by);
      element.click();
    } catch (Exception e) {
      driver.getPageSource();
      element = waitForClickable(by);
      element.click();
    }
    return element;
  }

  protected String getTextOf(WebElement element) {
    String text = null;
    try {
      text = element.getText();
    } catch (Exception e) {
      text = waitForVisibility(element).getText();
    }
    return text;
  }

  protected List<String> getTexts(List<WebElement> elements) {
    waitForVisibilityOfAll(elements);
    return elements.stream().map(WebElement::getText).collect(Collectors.toList());
  }

  protected String getAttribute(WebElement element, String attribute) {
    return waitForVisibility(element).getAttribute(attribute);
  }

  protected WebElement writeText(String text, WebElement inputElement) {
    if (text != null) {
      try {
        inputElement.sendKeys(text);
      } catch (Exception e) {
        waitForVisibility(inputElement).sendKeys(text);
      }
    }
    return inputElement;
  }

  protected WebElement writeTextByOneLetter(String text, WebElement inputElement) {
    Chars.asList(text.toCharArray())
        .forEach(character -> writeText(character.toString(), inputElement));
    return inputElement;
  }

  protected WebElement writeTextWithActions(String text, WebElement inputElement) {
    Actions actions = getActions();
    waitForVisibility(inputElement);
    actions.moveToElement(inputElement).click().pause(Duration.of(1, ChronoUnit.SECONDS))
        .sendKeys(text).build().perform();
    return inputElement;
  }

  protected WebElement setValueJs(String text, WebElement element) {
    if (text != null) {
      waitForVisibility(element);
      jsExecutor.executeScript("arguments[0].value='" + text + "';", element);
    }
    return element;
  }

  protected WebElement getElementByText(String text) {
    return wait.until(
        ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='" + text + "']")));
  }

  protected WebElement getElementByText(String text, String initialXpath) {
    return wait.until(ExpectedConditions
        .visibilityOfElementLocated(By.xpath(initialXpath + "[text()='" + text + "']")));
  }

  protected WebElement getElementContainsText(String text) {
    return wait.until(ExpectedConditions
        .visibilityOfElementLocated(By.xpath("//*[contains(text(),'" + text + "')]")));
  }

  protected WebElement getElementContainsText(String text, String initialXpath) {
    return wait.until(ExpectedConditions
        .visibilityOfElementLocated(By.xpath(initialXpath + "[contains(text(),'" + text + "')]")));
  }

  protected int getSizeWithTimeout(List<WebElement> webElements, int timeout) {
    int size;
    try {
      new WebDriverWait(driver, Duration.ofSeconds(waitTimeout))
          .until(ExpectedConditions.visibilityOfAllElements(webElements));
      size = webElements.size();
    } catch (TimeoutException e) {
      size = webElements.size();
    }
    return size;
  }

  protected void scrollIntoView(WebElement element) {
    scrollIntoView(element, false);
  }

  protected void scrollIntoView(WebElement element, boolean alignToTop) {
    jsExecutor.executeScript("arguments[0].scrollIntoView(" + alignToTop + ");", element);
  }

  protected void scrollToMiddleOfScreen(WebElement element) {
    String script =
        "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
            + "var elementTop = arguments[0].getBoundingClientRect().top;"
            + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
    jsExecutor.executeScript(script, element);
  }

  protected WebElement clickWithJs(WebElement element) {
    jsExecutor.executeScript("arguments[0].click();", waitForVisibility(element));
    return element;
  }

  protected WebElement waitForVisibility(WebElement element) {
    return wait.until(ExpectedConditions.visibilityOf(element));
  }

  protected WebElement waitForVisibility(By by) {
    return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
  }

  protected WebElement waitForPresence(WebElement element) {
    long startTime = System.currentTimeMillis();
    boolean flag = false;
    while (!flag) {
      try {
        element.getSize();
        flag = true;
        sleep(500);
      } catch (NoSuchElementException | StaleElementReferenceException | JsonException e) {
        sleep(500);
        if (isTimeout(startTime, waitTimeout)) {
          throw new TimeoutException("Waiting for presence of element failed. Waited for "
              + waitTimeout + " with 500ms milliseconds interval. \n" + e.getMessage());
        }
      }
    }
    return element;
  }

  protected WebElement waitForClickable(WebElement element) {
    return wait.until(ExpectedConditions.elementToBeClickable(element));
  }

  protected WebElement waitForClickable(By by) {
    return wait.until(ExpectedConditions.elementToBeClickable(by));
  }

  protected WebElement waitForEnabled(WebElement element) {
    wait.until((ExpectedCondition<Boolean>) driver -> element.isEnabled());
    return element;
  }

  protected void waitForUrlToContain(String text) {
    wait.until(ExpectedConditions.urlContains(text));
  }

  protected WebElement waitForInvisibility(WebElement element) {
    wait.until(ExpectedConditions.invisibilityOf(element));
    return element;
  }

  protected void waitToDisappear(WebElement element) {
    wait.until((ExpectedCondition<Boolean>) driver -> {
      sleep(100);
      try {
        return !element.isDisplayed();
      } catch (NoSuchElementException | StaleElementReferenceException | JsonException e) {
        return true;
      }
    });
  }

  protected void waitToDisappearAll(List<WebElement> elements) {
    try {
      waitForVisibilityOfAtLeast(elements, 1);
    } catch (StaleElementReferenceException ignored) {
    }
    waitForInvisibilityOfAll(elements);
  }

  protected List<WebElement> waitForVisibilityOfAll(List<WebElement> elementsList) {
    return wait.until(ExpectedConditions.visibilityOfAllElements(elementsList));
  }

  protected List<WebElement> waitForVisibilityOfAtLeast(List<WebElement> elementsList,
      int atLeastSize) {
    try {
      wait.until((ExpectedCondition<Boolean>) driver -> elementsList.stream()
          .filter(WebElement::isDisplayed).count() >= atLeastSize);
    } catch (TimeoutException e) {
      throw new TimeoutException("Waiting for list to contain at least " + atLeastSize
          + " elements failed after " + waitTimeout + " s");
    }
    return elementsList;
  }

  protected List<WebElement> getOnlyDisplayedElementsOfList(List<WebElement> list){
    return waitForVisibilityOfAtLeast(list, 1).stream()
            .filter(WebElement::isDisplayed)
            .collect(Collectors.toList());
  }

  protected boolean isDisplayedWithTimeout(WebElement element) {
    return isDisplayedWithTimeout(element, 10);
  }

  protected boolean isDisplayedWithTimeout(WebElement element, long timeOut) {
    long startTime = System.currentTimeMillis();
    while (!isTimeout(startTime, timeOut)) {
      try {
        if (element.isDisplayed()) {
          return true;
        }
        sleep(500);
      } catch (NoSuchElementException | StaleElementReferenceException | JsonException e) {
        sleep(500);
      }
    }
    return false;
  }

  protected boolean isDisplayedWithTimeout(By element, long timeOut) {
    long startTime = System.currentTimeMillis();
    while (!isTimeout(startTime, timeOut)) {
      try {
        if (driver.findElement(element).isDisplayed()) {
          return true;
        }
        sleep(500);
      } catch (NoSuchElementException | StaleElementReferenceException | JsonException e) {
        sleep(500);
      }
    }
    return false;
  }

  protected boolean isAnyElementDisplayed(List<WebElement> elementList, long timeOut) {
    long startTime = System.currentTimeMillis();
    while (!isTimeout(startTime, timeOut)) {
      try {
        if (!elementList.isEmpty()) {
          return true;
        }
        sleep(500);
      } catch (NoSuchElementException | StaleElementReferenceException | JsonException e) {
        sleep(500);
      }
    }
    return false;
  }

  protected boolean isEnabledWithTimeout(WebElement element, long timeOut) {
    long startTime = System.currentTimeMillis();
    while (!isTimeout(startTime, timeOut)) {
      try {
        if (element.isEnabled()) {
          return true;
        }
        sleep(500);
      } catch (NoSuchElementException | StaleElementReferenceException | JsonException e) {
        sleep(500);
      }
    }
    return false;
  }

  protected void waitForInvisibilityOfAll(List<WebElement> listOfElements) {
    wait.until((ExpectedCondition<Boolean>) driver -> {
      int flag = 0;
      for (WebElement element : listOfElements) {
        try {
          if (element.isDisplayed()) {
            flag = flag + 1;
          }
        } catch (NoSuchElementException | StaleElementReferenceException | JsonException e) {
        }
      }
      return flag == 0;
    });
  }

  protected void waitForJavascript() {
    try {
      wait.until((ExpectedCondition<Boolean>) driver -> jsExecutor
          .executeScript("return document.readyState").equals("complete"));
    } catch (Exception e) {
      throw new TimeoutException(
          "Waiting for JavaScript to complete loading failed after " + waitTimeout + " s");
    }
  }

  protected void waitForReact() {
    try {
      wait.until((ExpectedCondition<Boolean>) driver -> jsExecutor
          .executeScript("return window.document.hasHomeMounted").equals("complete"));
    } catch (Exception e) {
      throw new TimeoutException(
          "Waiting for JavaScript to complete loading failed after " + waitTimeout + " s");
    }
  }

  protected void waitForJQuery() {
    String isJQueryScript = "return window.jQuery !== undefined";
    int timeout = 15;
    try {
      new WebDriverWait(driver, Duration.ofSeconds(waitTimeout))
          .until((ExpectedCondition<Boolean>) driver -> {
            sleep(100);
            try {
              return (Boolean) jsExecutor.executeScript("return jQuery.active==0");
            } catch (Exception e) {
              return false;
            }
          });
    } catch (TimeoutException e) {
      if ((Boolean) jsExecutor.executeScript(isJQueryScript)) {
        throw new TimeoutException(
            "Waiting for jQuery to complete loading failed after " + timeout + " s");
      } else {
        AutomationLogger.getLogger()
            .warn("[WARNING] waitForJquery() method called on page without jquery implemented!");
      }
    }
  }

  protected void waitForAngular() {
    String isAngularScript = "return window.angular !== undefined";
    int timeout = 15;
    try {
      new WebDriverWait(driver, Duration.ofSeconds(waitTimeout))
          .until((ExpectedCondition<Boolean>) driver -> {
            sleep(100);
            try {
              return (Boolean) jsExecutor.executeScript(
                  "return angular.element(document).injector().get('$http').pendingRequests.length === 0");
            } catch (Exception e) {
              return false;
            }
          });
    } catch (TimeoutException e) {
      if ((Boolean) jsExecutor.executeScript(isAngularScript)) {
        throw new TimeoutException(
            "Waiting for angular to complete loading failed after " + timeout + " s");
      } else {
        AutomationLogger.getLogger().warn(
            "[WARNING] waitForAngular() method called on page without angular implemented!");
      }
    }
  }

  protected void waitForAjax() {
    jsExecutor.executeScript("var callback = arguments[arguments.length - 1];"
        + "var xhr = new XMLHttpRequest();" + "xhr.open('GET', '/Ajax_call', true);"
        + "xhr.onreadystatechange = function() {" + "  if (xhr.readyState == 4) {"
        + "    callback(xhr.responseText);" + "  }" + "};" + "xhr.send();");
  }

  protected void waitForJSAndJquery() {
    waitForJavascript();
    waitForJQuery();
  }

  protected Select getSelect(WebElement element) {
    return new Select(waitForVisibility(element));
  }

  protected WebElement selectByText(String text, WebElement dropdown) {
    if (text != null) {
      Select select = getSelect(dropdown);
      select.selectByVisibleText(text);
    }
    return dropdown;
  }

  protected void selectDropDownOptionByText(List<WebElement> dropdownOptions,
      String valueToBeSelected) {
    dropdownOptions.stream().filter(WebElement::isDisplayed)
        .filter(s -> s.getText().trim().equalsIgnoreCase(valueToBeSelected)).findFirst()
        .orElseThrow(() -> new RuntimeException(
            "Dropdown element with " + valueToBeSelected + " value has not been found"))
        .click();
  }

  protected Actions getActions() {
    return new Actions(driver);
  }

  protected void sleep(long milliseconds) {
    try {
      Thread.sleep(milliseconds);
    } catch (InterruptedException ignored) {
    }
  }

  protected void moveToElement(WebElement webElement) {
    waitForVisibility(webElement);
    scrollIntoView(webElement);
    getActions().moveToElement(webElement).build().perform();
  }

  protected void moveToElementAndClick(WebElement webElement) {
    waitForVisibility(webElement);
    scrollIntoView(webElement);
    getActions().moveToElement(webElement).click().build().perform();
  }

  protected WebElement clearAndWriteText(String text, WebElement inputElement) {
    if (text != null) {
      waitForClickable(inputElement);
      if (!inputElement.getAttribute("value").isEmpty()) {
        clickOn(inputElement);
        inputElement.clear();
        clearWithControlAndBackspace(inputElement);
      }
      inputElement.sendKeys(text);
    }
    return inputElement;
  }

  protected WebElement clear(WebElement inputElement) {
    waitForVisibility(inputElement);
    inputElement.clear();
    return inputElement;
  }

  protected WebElement clearWithBackspaceAndWrite(String text, WebElement inputElement) {
    waitForVisibility(inputElement);
    while (inputElement.getAttribute("value").isEmpty()) {
      inputElement.sendKeys(Keys.BACK_SPACE);
    }
    inputElement.sendKeys(text);
    return inputElement;
  }

  protected void clearWithControlAndBackspace(WebElement element) {
    String os = System.getProperty("os.name").toLowerCase();
    waitForVisibility(element);
    while (!element.getAttribute("value").isEmpty()) {
      setValueJs("", element);
      if (os.contains("mac")) {
        element.sendKeys(Keys.chord(Keys.COMMAND, "a"));
      } else {
        element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
      }
      element.sendKeys(Keys.BACK_SPACE);
    }
  }

  protected void waitForAllScripts() {
    waitForJavascript();
    waitForJQuery();
    waitForAngular();
  }

  protected void waitForAllScripts(int sleepTime) {
    waitForAllScripts();
    sleep(sleepTime);
  }

  protected void performIfDisplayed(WebElement element, Consumer<WebElement> action, long timeout) {
    if (isDisplayedWithTimeout(element, timeout)) {
      action.accept(element);
    }
  }

  protected void goToSubUrl(String subUrl) {
    jsExecutor.executeScript("window.location.assign('" + subUrl + "')");
    checkIsAtPage(subUrl);
  }

  protected void checkIsAtPage(String urlContains) {
    boolean flag;
    try {
      flag = wait.until(ExpectedConditions.urlContains(urlContains));
    } catch (TimeoutException e) {
      flag = false;
    }
    assertThat(flag).as("User was expected to be at page with url '" + urlContains
        + "' , but is at '" + driver.getCurrentUrl() + "'").isTrue();
  }

  protected void waitForStableHtml() {
    waitForJavascript();
    AtomicInteger i = new AtomicInteger();
    waitCustomCondition(() -> {
      String src = driver.getPageSource();
      sleep(100);
      if (src.equals(driver.getPageSource())) {
        i.getAndIncrement();
      } else {
        i.set(0);
      }
      return i.get() > 5;
    }, "Waiting for stable html");
  }

  public boolean waitCustomCondition(Supplier<Boolean> supplier, String message) {
    return waitCustomCondition(supplier, message, waitTimeout);
  }

  public boolean waitCustomCondition(Supplier<Boolean> supplier, String message, long timeout) {
    return new FluentWait<>(driver).withTimeout(Duration.ofSeconds(timeout))
        .pollingEvery(Duration.ofMillis(500)).withMessage(message)
        .until(((ExpectedCondition<Boolean>) driver -> supplier.get()));
  }

  private boolean isTimeout(long startTime, long desiredTimeoutInSeconds) {
    long start = startTime / 1000;
    return System.currentTimeMillis() / 1000 > start + desiredTimeoutInSeconds;
  }

  protected String getCurrentWindowHandle() {
    return driver.getWindowHandle();
  }

  protected void switchToDefaultFrame() {
    driver.switchTo().defaultContent();
  }

  protected String getCurrentFrameName() {
    JavascriptExecutor exe = (JavascriptExecutor) driver;
    return exe.executeScript("return self.name").toString();
  }

  protected void switchToFrame(String frameName) {
    driver.switchTo().frame(frameName);
  }

  protected void switchToFrame(WebElement element) {
    driver.switchTo().frame(element);
  }

  protected void switchToWindowHandle(String windowHandle) {
    driver.switchTo().window(windowHandle);
  }

  protected void screenshot(String saveDir) {
    try {
      File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
      File directory = new File(saveDir);
      FileUtils.copyFile(screenshot, directory);
    } catch (Exception e) {
      AutomationLogger.getLogger().error("Failed to take screenshot to path " + saveDir);
    }
  }

  protected Retry retry(WebElement element, Consumer<WebElement> action) {
    return new Retry(element, action);
  }

  protected void moveToElementAndDoubleClick(WebElement webElement) {
    waitForVisibility(webElement);
    scrollIntoView(webElement);
    getActions().moveToElement(webElement).doubleClick().build().perform();
  }

  protected int getIntFromElement(WebElement element) {
    return Integer.parseInt(getTextOf(element));
  }

  protected boolean checkListContainsElementWithText(List<WebElement> list, String value) {
    return list.stream().map(WebElement::getText).anyMatch(s -> s.contains(value));
  }

  protected WebElement getFirstVisibleElementFromList(List<WebElement> list) {
    return waitForVisibilityOfAtLeast(list, 1).stream().filter(WebElement::isDisplayed).findFirst()
        .get();
  }

  protected WebElement markCheckboxTo(boolean state, WebElement element) {
    waitForPresence(element);
    if (state) {
      if (!element.isSelected()) {
        element.click();
      }
    } else {
      if (element.isSelected()) {
        element.click();
      }
    }
    return element;
  }

  protected Alert waitForAlert() {
    waitCustomCondition(() -> {
      try {
        driver.switchTo().alert();
      } catch (NoAlertPresentException e) {
        return false;
      }
      return true;
    }, "Waiting for alert to be present", 60);
    return driver.switchTo().alert();
  }

  public abstract V verify();
}
