package ui.pages.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.assertions.HomePageAssertions;
import ui.pages.TimeModulePage;

public abstract class RangeHrmBasePage<V> extends BasePage<V> {

  @FindBy(css="p.oxd-userdropdown-name")
  protected WebElement userDropdown;
  @FindBy(xpath="//a[contains(@href, 'viewTimeModule')]")
  protected WebElement timeModuleLink;

  public RangeHrmBasePage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  public TimeModulePage goToTime(){
    clickOn(timeModuleLink);
    return new TimeModulePage(driver);
  }
}
