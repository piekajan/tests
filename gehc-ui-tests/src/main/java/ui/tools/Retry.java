package ui.tools;

import java.util.function.Consumer;
import java.util.function.Supplier;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

public class Retry {

    private Consumer<WebElement> action;
    private Supplier<Boolean> condition;
    private WebElement elementAction;
    private int attempts;

    public Retry(WebElement elementAction, Consumer<WebElement> action) {
        this.elementAction = elementAction;
        this.action = action;
    }

    public Retry until(Supplier<Boolean> condition) {
        this.condition = condition;
        return this;
    }

    public Retry attempts(int attempts) {
        this.attempts = attempts;
        return this;
    }

    public void perform() {
        if (attempts == 0) {
            attempts = 20;
        }
        int retries = 0;
        while (retries < attempts && !condition.get()) {
            action.accept(elementAction);
            retries++;
        }
        if (retries >= attempts) {
            throw new WebDriverException("Retry function failed after " + retries + " retries");
        }
    }


}
